from django import forms


class ContactForm(forms.Form):
    email = forms.EmailField(max_length=50, min_length=10)
    subject = forms.CharField(max_length=50, min_length=10)
    content = forms.CharField(max_length=500, min_length=10)
