#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, HttpResponseRedirect
from django.contrib import messages
from django.core.mail import send_mail
from django.shortcuts import redirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from contact.forms import ContactForm


def index(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            send_mail(form.cleaned_data['subject'], form.cleaned_data['content'], form.cleaned_data['email'], ['kotka5351@gmail.com'], fail_silently=True)
            form = ContactForm()
            message = "Wys�ano wiadomo��!"
            redirect(reverse('contact'))
    else:
        form = ContactForm()
    return render_to_response("contact.html", locals(), RequestContext(request))