from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

class Contact(CMSApp):
    name = _("Contact")
    urls = ["contact.urls"]
    #render_template = "contact.html"


apphook_pool.register(Contact)