from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
from menu import BlogMenu

class Blog(CMSApp):
    name = _("Blog")
    urls = ["microblog.urls"]
    menu = [BlogMenu]

apphook_pool.register(Blog)
