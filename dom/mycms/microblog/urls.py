from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.all_posts, name="all"),
    url(r'^user/(?P<idd>\d+)$', views.user_posts, name="user_posts"),
    url(r'^edit/(?P<idd>\d+)$', views.edit, name="edit"),
    url(r'^tagged/(?P<idd>\d+)$', views.tagged, name="tagged"),
    url(r'^users/activate/(?P<key>.+)$', views.activate, name="activate"),
    url(r'^login$', views.log_in, name="log_in"),
    url(r'^logout$', views.log_out, name="log_out"),
    url(r'^article/create$', views.create, name="create"),
    url(r'^register$', views.register, name="register"),
    url(r'^month/(?P<month_id>.+)$', views.month, name="month")
)
