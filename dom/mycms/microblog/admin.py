from django.contrib import admin
from microblog.models import Tag, Article, UserActivation
from django.contrib.auth.models import User

admin.site.register(Tag)
admin.site.register(UserActivation)


class ArticleAdmin(admin.ModelAdmin):
    list_filter = ('pub_date', )
    ordering = ('pub_date', )
    search_fields = ('tags__text',)


admin.site.register(Article, ArticleAdmin)


