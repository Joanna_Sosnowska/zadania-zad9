# -*- coding: utf-8 -*-
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as T
from django.contrib.auth.models import User
from microblog.models import Article
import datetime


class BlogMenu(CMSAttachMenu):
    name = T("Blog menu")

    def get_nodes(self, request):
        menu = []

        if request.user.is_authenticated():
            create = NavigationNode(u'Nowy wpis', reverse('create'), 1)
            menu.append(create)
            log_out = NavigationNode(u'Wyloguj', reverse('log_out'), 2)
            menu.append(log_out)
        else:
            log_in = NavigationNode(u'Zaloguj', reverse('log_in'), 3)
            menu.append(log_in)
            register = NavigationNode(u'Zarejestruj', reverse('register'), 4)
            menu.append(register)

        all = NavigationNode(u'Autorzy', reverse('all'), 5)
        menu.append(all)

        users = User.objects.all()

        for user in users:
            us = NavigationNode(user.username, reverse('user_posts', args=[user.pk]), user.pk + 500, 5)
            menu.append(us)

        months = NavigationNode(u'Miesiace', reverse('all'), 6)
        menu.append(months)

        months_ = Article.objects.raw("SELECT id, strftime('%%m', pub_date) as month, pub_date FROM microblog_article GROUP BY strftime('%%m', pub_date)")
        for item in months_:
            url = reverse('month', args=[item.month])
            menu.append(NavigationNode(datetime.datetime.strftime(item.pub_date, '%m'), url, item.id + 500, 6))

        return menu
menu_pool.register_menu(BlogMenu)
