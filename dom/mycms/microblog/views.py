#!/usr/bin/python
# -*- coding: utf-8 -*-
import random
import string
from django.shortcuts import render, get_list_or_404, redirect, render_to_response
from django.contrib import messages
from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from django.http import HttpResponseRedirect
from microblog.forms import LoginForm, ArticleForm, RegistrationForm
from django.forms.util import ErrorList
from django.contrib import messages
from django.core.mail import send_mail
from microblog.models import Article, Tag, UserActivation
from django.template import RequestContext


def all_posts(request, message=None):
    wpisy = Article.objects.order_by('-pub_date')
    title = "Wszystkie posty"
    return render(request, 'all.html', {'wpisy': wpisy, 'title': title, 'message': message})


def user_posts(request, idd):
    uzytkownik = User.objects.get(pk=idd)
    if uzytkownik:
        title = "Posty uzytkownika: '%s'" % uzytkownik.username
        wpisy = Article.objects.all().filter(user_id=idd).order_by('-pub_date')
        return render_to_response('all.html', locals(), context_instance=RequestContext(request))
    raise Http404


def tagged(request, idd):
    tag = Tag.objects.get(pk=idd)
    if tag:
        title = "Posty tagu: '%s'" % tag.text
        wpisy = Article.objects.all().filter(tags__pk=idd).order_by('-pub_date')
        return render_to_response('all.html', locals(), context_instance=RequestContext(request))
    raise Http404


def create(request):
    if not request.user.is_authenticated():
        raise Http404()
    if request.method == "POST":
        form = ArticleForm(request.POST)
        if form.is_valid():
            post = Article(
                tytul=form.cleaned_data['tytul'],
                tresc=form.cleaned_data['tresc'],
                pub_date=timezone.now(),
                user=request.user
            )
            post.save()
            for tag in form.cleaned_data['tags']:
                post.tags.add(tag)
            post.save()
            return redirect('all')
    else:
        form = ArticleForm()
    raise Http404()



def activate(request, key):
    if request.user.is_authenticated():
        raise Http404()
    profil = UserActivation.objects.get(key=key)
    if profil:
        uzytkownik = User.objects.get(pk=profil.user.id)
        uzytkownik.is_active = True
        uzytkownik.save()
        profil.delete()
        messages.info(request, 'Konto jest teraz aktywne')
        return redirect('all')
    raise Http404()


def edit(request, idd):
    art = Article.objects.get(pk=idd)
    editable = request.user.groups.filter(name='moderator') or (art.user.id == request.user.id)
    if art and request.user.is_authenticated and editable:
        if request.method == "POST":
            form = ArticleForm(request.POST)
            if form.is_valid():
                art.tytul = form.cleaned_data['tytul']
                art.tresc = form.cleaned_data['tresc']
                art.tags.clear()
                for tag in form.cleaned_data['tags']:
                    art.tags.add(tag)
                art.mod_date = timezone.now()
                art.user2 = request.user
                art.save()
                messages.info(request, 'Zapisano zmiany')
                return redirect('all')
        else:
            form = ArticleForm(initial={'tytul': art.tytul, 'tags': art.tags.all(), 'tresc': art.tresc})
        return render_to_response('edit.html', locals(), context_instance=RequestContext(request))
    messages.info(request, 'Nie edytuj, to nie twoje')
    return redirect('all')



def register(request):
    if request.user.is_authenticated():
        raise Http404()

    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            uzytkownik = User.objects.create(
                username=form.cleaned_data['login'],
                email=form.cleaned_data['email'],
                is_active=False
            )
            uzytkownik.set_password(form.cleaned_data['password1'])
            uzytkownik.save()
            profile = UserActivation(user=uzytkownik, key=''.join(random.sample(string.lowercase+string.digits,10)))
            profile.save()
            link = "http://194.29.175.240/~p13/wsgi/blog/users/activate/%s " % profile.key
            send_mail('Link aktywacyjny', link, 'kotka5351@gmail.com', [form.cleaned_data['email']])
            messages.info(request, 'Zarejestrowano. Odbierz maila i kliknij link aktywacyjny.')
            return redirect('all')
    else:
        form = RegistrationForm()
    return render_to_response('register.html', locals(), context_instance=RequestContext(request))


def month(request, month_id):
    title = "Posty z miesiaca: " + month_id
    wpisy = Article.objects.raw("SELECT * FROM microblog_article WHERE strftime('%%m', pub_date)='" + month_id + "'")
    logged = True
    return render_to_response('all.html', locals(), context_instance=RequestContext(request))


def log_in(request):
    if request.user.is_authenticated():
        raise Http404()
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            uzytkownik = authenticate(
                username=form.cleaned_data['login'],
                password=form.cleaned_data['password']
            )
            if uzytkownik:
                if uzytkownik.is_active:
                    login(request, uzytkownik)
                    messages.info(request, 'Zalogowano')
                else:
                    messages.error(request, 'U�ytkownik nieaktywny. Aktywuj konto')
                return redirect('all')
            else:
                 messages.info(request, 'B��dny login lub has�o')
    else:
        form = LoginForm()
    return render_to_response('login.html', locals(), context_instance=RequestContext(request))


def log_out(request):
    if request.user.is_authenticated():
        logout(request)
        messages.info(request, 'Wylogowano')
    else:
        raise Http404()
    return redirect('all')