from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from cms.models.pluginmodel import CMSPlugin


class Comment(models.Model):
        author = models.CharField(max_length=50)
        text = models.CharField(max_length=500)
        date = models.DateTimeField(default=timezone.now())

        def __unicode__(self):
                return self.text


class Comments(CMSPlugin):
        comments = models.ManyToManyField(Comment, blank=True, null=True)