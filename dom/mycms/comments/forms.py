from django import forms


class CommentsForm(forms.Form):
    author = forms.CharField(max_length=100, min_length=3)
    text = forms.CharField(max_length=1000, min_length=5)
